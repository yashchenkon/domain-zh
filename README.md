### Solution
You can find diagram attached to the repo, also you can find it by following the next link (it's more interactive):

https://dbdiagram.io/d/5df65550edf08a25543f19ab

### Task

Please create the domain model for the following use cases: 
1) A landlord can rent out a separate apartment, the whole building or several apartments to another party. 
2) A landlord can sign a rental contract with one or multiple tenants. 
3) One tenant can rent multiple apartments from the same landlord. 
4) One tenant can rent multiple apartments simultaneously. 
5) A landlord can also be a tenant of another landlord. 

We suggest ER diagram as an outcome of this task but you can choose a format that makes more sense for you. 

### Assumptions

1. Each apartment has only a single owner.
2. Building is not own by anybody. If building has several apartments, each apartment has own owner. If all apartments in the
building belong to a single owner, all of them just refer to a single landlord.
3. All users of the system are persons, but only few of them landlords. Landlord - it's just a set of additional information.
4. Renting of the whole building it's just renting of all apartments.
5. This schema doesn't cares about the history of changes. If we want to see the history, we need to change the schema in some
way to store the history of changes.
6. If owner of apartment is changed, all existing contracts / offers will belongs to a new owner.